import axiosClient from './axiosClient';

const messageApi = {
	getMessageByChannel: (channelId) => {
		return axiosClient({
			method: 'GET',
			url: `/message/${channelId}`,
		});
	},
};

export default messageApi;
