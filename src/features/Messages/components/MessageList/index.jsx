import {
	Avatar,
	Box,
	Grid,
	Paper,
	Tooltip,
	Typography,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import React from 'react';
import { getCookie } from 'utils/function';
import { useStyles } from './MessageList';

MessageList.propTypes = {
	messages: PropTypes.array.isRequired,
};

function MessageList(props) {
	const { messages } = props;
	const classes = useStyles();

	const userId = getCookie('c_user');

	return (
		<Box className={classes.root}>
			{messages.map((message, index) =>
				message.sender_id === userId ? (
					<Grid key={index} container className={classes.seftPanel}>
						<Grid item md={6}></Grid>
						<Grid item md={6}>
							<Grid container justifyContent='flex-end'>
								<Paper
									elevation={0}
									style={{
										width: 'fit-content',
									}}
									className={classes.chatSeft}
									key={index}
								>
									<Typography variant='body2' style={{ color: 'white' }}>
										{message.content}
									</Typography>
								</Paper>
							</Grid>
						</Grid>
					</Grid>
				) : (
					<Grid key={index} container className={classes.notSeftPanel}>
						<Grid item md={6}>
							<Grid container alignItems='center'>
								<Grid item md={1}>
									<Tooltip title={message.display_name}>
										<Avatar
											style={{ width: '2rem', height: '2rem' }}
											src={message.avt_link}
										/>
									</Tooltip>
								</Grid>
								<Grid item md={11}>
									<Paper
										elevation={0}
										style={{
											width: 'fit-content',
										}}
										className={classes.chatNotSeft}
										key={index}
									>
										<Typography variant='body2' color='inherit'>
											{message.content}
										</Typography>
									</Paper>
								</Grid>
							</Grid>
						</Grid>
						<Grid item md={6}></Grid>
					</Grid>
				)
			)}
		</Box>
	);
}

export default MessageList;
