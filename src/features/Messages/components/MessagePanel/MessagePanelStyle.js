const { makeStyles } = require('@material-ui/core');

export const useStyles = makeStyles(() => ({
	messageList: {
		height: '84.5%',
		width: '100%',
		overflowY: 'scroll',
	},
}));
