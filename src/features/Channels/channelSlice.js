import { createSlice } from '@reduxjs/toolkit';

const channelSlice = createSlice({
	name: 'channels',
	initialState: {
		isChannelByUserChange: false,
		isChannelsChange: false,
		isChannelDetailChange: false,
		loading: false,
		error: '',
	},
	reducers: {
		handleChannelsByUserChange: (state) => {
			state.isChannelByUserChange = !state.isChannelByUserChange;
		},
		handleChannelsChange: (state) => {
			state.isChannelsChange = !state.isChannelsChange;
		},
		handleChannelDetailChange: (state) => {
			state.isChannelDetailChange = !state.isChannelDetailChange;
		},
	},
});

const { actions, reducer: channelReducer } = channelSlice;
export const {
	handleChannelsByUserChange,
	handleChannelsChange,
	handleChannelDetailChange,
} = actions;
export default channelReducer;
