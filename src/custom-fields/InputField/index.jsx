import { FormControl, FormHelperText, TextField } from '@material-ui/core';
import { ErrorMessage } from 'formik';
import PropTypes from 'prop-types';
import React from 'react';

InputField.propTypes = {
	label: PropTypes.string,
	autoFocus: PropTypes.bool,
	fullWidth: PropTypes.bool,
	required: PropTypes.bool,
	type: PropTypes.string,
};

InputField.defaultProps = {
	label: '',
	autoFocus: false,
	fullWidth: false,
	required: false,
	type: 'text',
};

function InputField(props) {
	const { field, form, type, label, required, fullWidth, autoFocus } = props;

	const { name } = field;
	const { errors, touched } = form;
	const showError = errors[name] && touched[name];

	return (
		<FormControl fullWidth error={showError}>
			<TextField
				id={name}
				{...field}
				label={label}
				type={type}
				variant='outlined'
				margin='normal'
				required={required}
				fullWidth={fullWidth}
				autoFocus={autoFocus}
				error={showError}
			/>
			<ErrorMessage name={name} component={FormHelperText} />
		</FormControl>
	);
}

export default InputField;
