import { Grid } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import imageSignin from 'assets/images/imageSignin.jpg';
import React from 'react';
import { Helmet } from 'react-helmet';
import './SignIn.css';
import { useStyles } from './SignInStyle';

export default function SignIn() {
	const classes = useStyles();

	const openGoogleLogin = () => {
		window.open(`${process.env.REACT_APP_API_URL}/auth/google`, '_self');
	};

	return (
		<>
			<Helmet>
				<title>Sign in | Huvi</title>
			</Helmet>
			<div
				style={{
					position: 'relative',
					backgroundImage: `url(${imageSignin})`,
					backgroundRepeat: 'no-repeat',
					backgroundSize: 'cover',
					backgroundPosition: 'center',
					width: '100vw',
					height: '100vh',
					filter: 'brightness(80%)',
				}}
			></div>
			<div
				style={{
					position: 'fixed',
					top: '50%',
					left: '50%',
					transform: 'translate(-50%, -50%)',
					zIndex: '10000',
				}}
			>
				<Typography
					component='h1'
					variant='h1'
					style={{ color: '#fff', marginBottom: '1rem' }}
				>
					Welcome to Huvi !
				</Typography>
				<Grid container justifyContent='center'>
					<Card
						className={classes.root}
						style={{ width: 'fit-content', cursor: 'pointer' }}
						onClick={openGoogleLogin}
					>
						<CardMedia
							className={classes.cover}
							image='https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg'
							title='Live from space album cover'
						/>
						<CardContent className={classes.content}>
							<Typography component='h6' variant='h6'>
								Sign in with Google
							</Typography>
						</CardContent>
					</Card>
				</Grid>
			</div>
		</>
	);
}
