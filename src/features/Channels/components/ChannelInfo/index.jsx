import {
	Avatar,
	Badge,
	Divider,
	Grid,
	IconButton,
	Modal,
	Paper,
	Tooltip,
	Typography,
	withStyles,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import InfoIcon from '@material-ui/icons/Info';
import channelApi from 'api/channelApi';
import userApi from 'api/userApi';
import {
	handleChannelDetailChange,
	handleChannelsByUserChange,
} from 'features/Channels/channelSlice';
import PropTypes from 'prop-types';
import React, { useContext, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { ToastifyContext } from 'utils/ToastifyConfig';
import ChangeChannelAvatarModal from '../ChangeChannelAvatarModal';
import './ChannelInfo.css';

ChannelInfo.propTypes = {
	channelInfo: PropTypes.object.isRequired,
};

const SmallAvatar = withStyles((theme) => ({
	root: {
		width: 22,
		height: 22,
		padding: '0.9rem',
	},
}))(Avatar);

function ChannelInfo({ channelInfo }) {
	const notify = useContext(ToastifyContext);
	const dispatch = useDispatch();

	const [open, setOpen] = useState(false);
	const [users, setUsers] = useState([]);
	const [host, setHost] = useState({});

	const handleOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const handleChangeChannelImg = (data) => {
		if (channelInfo) {
			const post = async () => {
				try {
					const response = await channelApi.changeChannelImg(
						data,
						channelInfo.channel_id
					);
					dispatch(handleChannelsByUserChange());
					dispatch(handleChannelDetailChange());
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/signin');
					}
				}
			};

			post();
		}
	};

	useEffect(() => {
		if (channelInfo && channelInfo.channel_id) {
			const get = async () => {
				try {
					const response = await userApi.getUsersByChannel(
						channelInfo.channel_id
					);

					const hostInfo = response.find(
						(r) => r.user_id === channelInfo.user_id
					);
					setUsers(response);
					setHost(hostInfo);
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/signin');
					}
				}
			};

			get();
		}
	}, [channelInfo]);

	return (
		channelInfo && (
			<Grid container justifyContent='flex-end'>
				<Tooltip title='Channel Information'>
					<IconButton onClick={handleOpen} style={{ padding: '0.25rem' }}>
						<InfoIcon color='primary' fontSize='large' />
					</IconButton>
				</Tooltip>
				<Modal open={open} onClose={handleClose}>
					<Paper className='create-modal'>
						<Grid container alignItems='center'>
							<Grid item md={2}></Grid>
							<Grid item xs={8} md={8} lg={8} container justifyContent='center'>
								<Typography variant='h5'>Channel Info</Typography>
							</Grid>
							<Grid item xs={2} md={2} lg={2} container justify='flex-end'>
								<IconButton onClick={handleClose}>
									<CloseIcon />
								</IconButton>
							</Grid>
							<Grid item xs={12} md={12} lg={12}>
								<Divider />
							</Grid>
						</Grid>
						<Grid container>
							<Grid container style={{ marginBottom: '1rem' }}></Grid>
							<Grid container>
								<Grid container justifyContent='center'>
									<Grid container justify='center'>
										<Badge
											overlap='circle'
											anchorOrigin={{
												vertical: 'bottom',
												horizontal: 'right',
											}}
											badgeContent={
												<SmallAvatar>
													<ChangeChannelAvatarModal
														handleChangeChannelImg={handleChangeChannelImg}
													/>
												</SmallAvatar>
											}
										>
											<Avatar
												style={{
													width: '8rem',
													height: '8rem',
													boxShadow:
														'0 16px 38px -12px rgb(0 0 0 / 16%), 0 4px 25px 0 rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%)',
												}}
												src={channelInfo.channel_avt}
											/>
										</Badge>
									</Grid>
								</Grid>
								<Grid
									container
									justifyContent='center'
									style={{ marginTop: '0.75rem' }}
								>
									<Typography
										style={{
											fontSize: '1.1rem',
											fontWeight: 'bold',
										}}
									>
										{channelInfo.channel_name}
									</Typography>
								</Grid>
							</Grid>
						</Grid>
						<Grid container style={{ marginTop: '2rem' }}>
							<Grid container style={{ marginBottom: '1rem' }}>
								<Typography variant='h6' style={{ fontWeight: 'bold' }}>
									Members ({users.length})
								</Typography>
							</Grid>
							{host && (
								<Grid
									container
									alignItems='center'
									style={{ marginBottom: '1rem' }}
								>
									<Grid item md={1}>
										<Avatar src={host.avt_link} />
									</Grid>
									<Grid item md={9}>
										<Typography style={{ marginLeft: '1rem' }}>
											{host.display_name} <i>({host.email})</i>
											&nbsp;&nbsp;<b>Host</b>
										</Typography>
									</Grid>
								</Grid>
							)}
							{users.length > 0 &&
								users.map(
									(user, index) =>
										user.user_id !== channelInfo.user_id && (
											<Grid
												container
												alignItems='center'
												style={{ marginBottom: '1rem' }}
											>
												<Grid item md={1}>
													<Avatar src={user.avt_link} />
												</Grid>
												<Grid item md={9}>
													<Typography style={{ marginLeft: '1rem' }}>
														{user.display_name} <i>({user.email})</i>
													</Typography>
												</Grid>
											</Grid>
										)
								)}
						</Grid>
					</Paper>
				</Modal>
			</Grid>
		)
	);
}

export default ChannelInfo;
