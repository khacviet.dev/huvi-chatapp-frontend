import axiosClient from './axiosClient';

const authApi = {
	signout: () => {
		return axiosClient({
			method: 'POST',
			url: '/auth/signout',
		});
	},
};

export default authApi;
