import axiosClient from './axiosClient';

const channelApi = {
	getChannels: () => {
		return axiosClient({
			method: 'GET',
			url: '/channel',
		});
	},
	getChannelsByUserId: () => {
		return axiosClient({
			method: 'GET',
			url: '/channel/me',
		});
	},
	getChannelsNotByUserId: () => {
		return axiosClient({
			method: 'GET',
			url: '/channel/notme',
		});
	},
	getChannelById: (channelId) => {
		return axiosClient({
			method: 'GET',
			url: `/channel/${channelId}`,
		});
	},
	getRequestsChannel: () => {
		return axiosClient({
			method: 'GET',
			url: `/channel/request`,
		});
	},
	createChannel: (data) => {
		return axiosClient({
			method: 'POST',
			url: `/channel/create`,
			data: data,
		});
	},
	requestChannel: (channelId) => {
		return axiosClient({
			method: 'POST',
			url: `/channel/request/${channelId}`,
		});
	},
	unRequestChannel: (channelId) => {
		return axiosClient({
			method: 'POST',
			url: `/channel/unrequest/${channelId}`,
		});
	},
	approveRequestChannel: (data, channelId) => {
		return axiosClient({
			method: 'POST',
			url: `/channel/approve/${channelId}`,
			data: data,
		});
	},
	leaveChannel: (channelId) => {
		return axiosClient({
			method: 'POST',
			url: `/channel/leave/${channelId}`,
		});
	},

	changeChannelImg: (data, channelId) => {
		return axiosClient({
			method: 'PATCH',
			url: `/channel/${channelId}`,
			data: data,
		});
	},
};

export default channelApi;
