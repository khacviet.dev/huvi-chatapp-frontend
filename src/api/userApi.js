import axiosClient from './axiosClient';

const userApi = {
	getMe: () => {
		return axiosClient({
			method: 'GET',
			url: '/user/me',
		});
	},
	getUsersByChannel: (channelId) => {
		return axiosClient({
			method: 'GET',
			url: `/user/${channelId}`,
		});
	},
};

export default userApi;
