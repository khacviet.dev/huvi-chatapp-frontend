import { configureStore } from '@reduxjs/toolkit';
import userReducer from 'features/Users/userSlice';
import channelReducer from 'features/Channels/channelSlice';

const rootReducer = {
	users: userReducer,
	channels: channelReducer,
};

const store = configureStore({
	reducer: rootReducer,
});

export default store;
