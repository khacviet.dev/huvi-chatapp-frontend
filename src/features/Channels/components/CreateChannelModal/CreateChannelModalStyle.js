const { makeStyles } = require('@material-ui/core');

export const useStyles = makeStyles(() => ({
	createPostTitle: {
		marginLeft: '90px',
		fontSize: '1.25rem',
		fontWeight: 'bold',
	},
	postPanel: {
		marginTop: '1rem',
	},
}));
