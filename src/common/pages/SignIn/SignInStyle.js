const { makeStyles } = require('@material-ui/core');

export const useStyles = makeStyles(() => ({
	root: {
		display: 'flex',
		alignItems: 'center',
		padding: '0.25rem',
	},
	content: {
		flex: '1 0 auto',
	},
	cover: {
		width: '60px',
		height: '60px',
	},
}));
