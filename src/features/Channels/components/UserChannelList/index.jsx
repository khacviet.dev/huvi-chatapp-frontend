import { Avatar, Badge, Grid, Paper, Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import React, { useContext, useEffect, useState } from 'react';
import { WebSocketContext } from 'utils/socketConfig';
import CreateChannelModal from '../CreateChannelModal';

UserChannelList.propTypes = {
	hadleCurrentChannelChange: PropTypes.func.isRequired,
	currentChannel: PropTypes.string.isRequired,
	channelsByUser: PropTypes.array.isRequired,
};

function UserChannelList({
	hadleCurrentChannelChange,
	currentChannel,
	channelsByUser,
}) {
	const socket = useContext(WebSocketContext);

	const [channelNotRead, setChannelNotRead] = useState({});
	const [haveMessage, setHaveMessage] = useState(false);

	useEffect(() => {
		if (channelsByUser) {
			socket.emit('getIsRead', true);

			socket.on('getIsReadResponse', (isReadStatus) => {
				setChannelNotRead(isReadStatus);
				socket.off('getIsReadResponse');
			});
		}
	}, [channelsByUser, haveMessage]);

	useEffect(() => {
		if (channelsByUser) {
			socket.on('haveMessage', () => {
				setHaveMessage(!haveMessage);
				socket.off('haveMessage');
			});
		}
	}, [channelsByUser, haveMessage]);

	return (
		<Paper
			style={{
				height: 'calc(100vh - 64px)',
				padding: '0rem 0.5rem',
				overflowY: 'scroll',
			}}
		>
			<Grid container style={{ marginTop: '1.5rem' }}>
				<Grid item md={12} style={{ marginBottom: '2rem' }}>
					<CreateChannelModal />
				</Grid>
				{channelsByUser.map((cbu, index) => (
					<Grid
						key={index}
						item
						md={12}
						style={{
							padding: '0.5rem',
							marginBottom: '1rem',
							cursor: 'pointer',
							backgroundColor:
								cbu.channel_id === currentChannel ? '#eee' : '#fff',
						}}
						className='panel'
						onClick={() => hadleCurrentChannelChange(cbu.channel_id)}
					>
						<Grid container alignItems='center'>
							<Grid item md={2}>
								<Avatar src={cbu.channel_avt} />
							</Grid>
							<Grid item md={9}>
								<Typography style={{ marginLeft: '1rem' }}>
									{cbu.channel_name}
								</Typography>
							</Grid>
							{channelNotRead && channelNotRead[cbu.channel_id] === true && (
								<Grid item md={1}>
									<Badge variant='dot' color='secondary' />
								</Grid>
							)}
						</Grid>
					</Grid>
				))}
			</Grid>
		</Paper>
	);
}

export default UserChannelList;
