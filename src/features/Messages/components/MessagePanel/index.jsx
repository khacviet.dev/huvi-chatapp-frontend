import {
	Avatar,
	Divider,
	Grid,
	Paper,
	TextField,
	Tooltip,
	Typography,
} from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';
import channelApi from 'api/channelApi';
import 'csspin/csspin.css';
import ChannelInfo from 'features/Channels/components/ChannelInfo';
import PropTypes from 'prop-types';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { WebSocketContext } from 'utils/socketConfig';
import { ToastifyContext } from 'utils/ToastifyConfig';
import MessageList from '../MessageList';
import { useStyles } from './MessagePanelStyle';

MessagePanel.propTypes = {
	currentChannel: PropTypes.string.isRequired,
};

function MessagePanel({ currentChannel }) {
	const notify = useContext(ToastifyContext);
	const classes = useStyles();
	const socket = useContext(WebSocketContext);

	const me = useSelector((state) => state.users.me);
	const isChannelDetailChange = useSelector(
		(state) => state.channels.isChannelDetailChange
	);
	const [currentChannelInfo, setCurrentChannelInfo] = useState({});

	const [messages, setMessages] = useState(() => {
		return [];
	});

	const [value, setValue] = useState('');
	const [over, setOver] = useState(() => false);

	const scrollEL = useRef(null);

	const handleInputChange = (e) => {
		setValue(e.target.value);
	};

	const handleSendMessage = () => {
		const messageObject = {
			channelId: currentChannel,
			message: value,
			avt: me.avt_link,
			displayName: me.display_name,
		};

		socket.emit('sendMessage', messageObject);
		setValue('');
	};

	const handleEnterSendMessage = (e) => {
		if (e.key === 'Enter' || e.keyCode === 13) {
			const messageObject = {
				channelId: currentChannel,
				message: value,
				avt: me.avt_link,
				displayName: me.display_name,
			};

			socket.emit('sendMessage', messageObject);
			setValue('');
		}
	};

	const loadMessagePrev = (e) => {
		if (e.target.scrollTop < 2 && !over) {
			e.target.scrollTop = 2;
			const infoObject = {
				channelId: currentChannel,
				skip: messages.length,
				st: e.target.scrollHeight,
			};
			socket.emit('getMessages', infoObject);
		}
	};

	useEffect(() => {
		if (currentChannel) {
			const get = async () => {
				try {
					const response = await channelApi.getChannelById(currentChannel);
					setCurrentChannelInfo(response);
				} catch (error) {
					notify('error', error.response.data.message);
					if (error.response.status === 403) {
						history.push('/signin');
					}
				}
			};

			get();
		}
	}, [currentChannel, isChannelDetailChange]);

	useEffect(() => {
		if (currentChannel) {
			setMessages([]);
		}
	}, [currentChannel]);

	useEffect(() => {
		if (currentChannel) {
			if (messages.length > 0) {
				scrollEL.current.scrollTop = scrollEL.current.scrollHeight;
			}
		}
	}, [currentChannel]);

	useEffect(() => {
		if (currentChannel) {
			if (messages.length < 1) {
				const infoObject = {
					channelId: currentChannel,
				};
				socket.emit('getMessagesFirst', infoObject);
				socket.on('getMessagesFirstResponse', (msg) => {
					setMessages(msg);
					scrollEL.current.scrollTop = scrollEL.current.scrollHeight;
				});

				return () => {
					socket.off('getMessagesFirstResponse');
				};
			}
		}
	}, [currentChannel]);

	useEffect(() => {
		if (currentChannel) {
			socket.on('getMessagesResponse', (msg, st) => {
				if (msg.length > 0) {
					const newMessageList = msg.concat(messages);
					setMessages(() => {
						return newMessageList;
					});
					scrollEL.current.scrollTop = scrollEL.current.scrollHeight - st;
				} else {
					setOver(true);
				}
			});

			return () => {
				socket.off('getMessagesResponse');
			};
		}
	}, [currentChannel, messages]);

	useEffect(() => {
		if (currentChannel) {
			socket.on('sendMessageResponse', (msg, channelId) => {
				if (currentChannel === channelId) {
					messages.push(msg);
					const newMessageList = [...messages];
					setMessages(newMessageList);
					scrollEL.current.scrollTop = scrollEL.current.scrollHeight;
				}
			});

			return () => {
				socket.off('sendMessageResponse');
			};
		}
	}, [currentChannel, messages]);

	return (
		<Paper style={{ height: 'calc(100vh - 64px)' }}>
			<Grid container style={{ height: '10%' }}>
				{currentChannelInfo && (
					<Grid
						container
						alignItems='center'
						style={{ padding: '0.75rem 1.5rem 0.75rem 0.75rem' }}
					>
						<Grid item md={8}>
							<Grid container alignItems='center'>
								<Avatar
									src={currentChannelInfo.channel_avt}
									style={{ width: '3rem', height: '3rem' }}
								/>
								<Grid item style={{ marginLeft: '1rem' }}>
									<Typography
										style={{ fontWeight: 'bold', fontSize: '1.1rem' }}
									>
										{currentChannelInfo.channel_name}
									</Typography>
								</Grid>
							</Grid>
						</Grid>
						<Grid item md={4}>
							<ChannelInfo channelInfo={currentChannelInfo} />
						</Grid>
					</Grid>
				)}
				<Grid container>
					<Divider style={{ width: '100%' }} />
				</Grid>
			</Grid>
			<Grid
				onScroll={(e) => loadMessagePrev(e)}
				ref={scrollEL}
				container
				className={classes.messageList}
			>
				<Grid container>
					{messages && messages.length > 0 && (
						<MessageList messages={messages} />
					)}
				</Grid>
			</Grid>
			<Grid container style={{ height: '5%', padding: '0 0.5rem' }}>
				<Grid container alignItems='center'>
					<TextField
						style={{ width: '95%' }}
						variant='outlined'
						size='small'
						label='Aa'
						onKeyUp={handleEnterSendMessage}
						value={value}
						onChange={handleInputChange}
					/>
					<Tooltip title='Send'>
						<SendIcon
							style={{ width: '5%', padding: '0', cursor: 'pointer' }}
							fontSize='large'
							color='primary'
							onClick={handleSendMessage}
						/>
					</Tooltip>
				</Grid>
			</Grid>
		</Paper>
	);
}

export default MessagePanel;
