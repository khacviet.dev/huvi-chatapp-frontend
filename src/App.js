import { Box, CircularProgress } from '@material-ui/core';
import Homepage from 'common/pages/HomePage';
import SignIn from 'common/pages/SignIn';
import 'fontsource-roboto';
import { Suspense, useState } from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';
import PrivateRoute from 'utils/PrivateRoute';
import { createTheme, ThemeProvider } from '@material-ui/core/styles';
import PublicRoute from 'utils/PublicRoute';
import teal from '@material-ui/core/colors/teal';
import ToastifyProvider from 'utils/ToastifyConfig';

function App() {
	const [darkMode, setDarkMode] = useState(false);

	const theme = createTheme({
		palette: {
			primary: teal,
			type: darkMode ? 'dark' : 'light',
		},
	});

	return (
		<ThemeProvider theme={theme}>
			<Box className='App'>
				<Suspense fallback={<CircularProgress />}>
					<BrowserRouter>
						<ToastifyProvider>
							<Switch>
								<PrivateRoute component={Homepage} path='/' exact />
								<PublicRoute
									restricted={true}
									component={SignIn}
									path='/signin'
									exact
								/>
							</Switch>
						</ToastifyProvider>
					</BrowserRouter>
				</Suspense>
			</Box>
		</ThemeProvider>
	);
}

export default App;
