const { makeStyles } = require('@material-ui/core');

export const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
	},
	seftPanel: {
		padding: '0.75rem',
	},
	notSeftPanel: {
		padding: '0.5rem',
	},
	chatSeft: {
		backgroundColor: theme.palette.primary.main,
		padding: '0.5rem',
		wordBreak: 'break-word',
	},
	chatNotSeft: {
		padding: '0.5rem',
		wordBreak: 'break-word',
		backgroundColor: '#eee',
	},
}));
