import Box from '@material-ui/core/Box';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import channelApi from 'api/channelApi';
import MyAppBar from 'common/components/MyAppBar';
import ChannelList from 'features/Channels/components/ChannelList';
import ChannelNotBelong from 'features/Channels/components/ChannelNotBelong';
import UserChannelList from 'features/Channels/components/UserChannelList';
import MessagePanel from 'features/Messages/components/MessagePanel';
import { getMe } from 'features/Users/userSlice';
import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch, useSelector } from 'react-redux';
import WebSocketProvider from 'utils/socketConfig';
import { ToastifyContext } from 'utils/ToastifyConfig';
import { useStyles } from './HomePageStyle';

export default function Homepage() {
	const classes = useStyles();
	const notify = useContext(ToastifyContext);

	const isChannelByUserChange = useSelector(
		(state) => state.channels.isChannelByUserChange
	);
	const users = useSelector((state) => state.users);
	const dispatch = useDispatch();

	const [isLoading, setIsLoading] = useState(false);
	const [channels, setChannels] = useState([]);
	const [channelsByUser, setChannelByUser] = useState([]);
	const [currentChannel, setCurrentChannel] = useState('');

	const hadleCurrentChannelChange = (channelId) => {
		setCurrentChannel(channelId);
	};

	useEffect(() => {
		dispatch(getMe());
	}, []);

	useEffect(() => {
		const get = async () => {
			try {
				setIsLoading(true);
				const response = await channelApi.getChannels();
				setIsLoading(false);
				setChannels(response);
			} catch (error) {
				setIsLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/signin');
				}
			}
		};

		get();
	}, []);

	useEffect(() => {
		const get = async () => {
			try {
				setIsLoading(true);
				const response = await channelApi.getChannelsByUserId();
				setIsLoading(false);
				setChannelByUser(response);
				if (response.length > 0) {
					setCurrentChannel(response[0].channel_id);
				}
			} catch (error) {
				setIsLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/signin');
				}
			}
		};

		get();
	}, [isChannelByUserChange]);

	return (
		<WebSocketProvider>
			<Helmet>
				<title>Huvi</title>
			</Helmet>
			{isLoading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-bubble'
				></div>
			)}
			<Box className={classes.root}>
				<CssBaseline />
				{/* header */}
				<MyAppBar me={users.me} />
				{/* main */}
				<main className={classes.content}>
					<Box className={classes.appBarSpacer} />
					{channelsByUser.length > 0 ? (
						<Grid container>
							<Grid item xs={2} md={2} lg={2}>
								<UserChannelList
									hadleCurrentChannelChange={hadleCurrentChannelChange}
									currentChannel={currentChannel}
									channelsByUser={channelsByUser}
								/>
							</Grid>
							<Grid item xs={7} md={7} lg={7}>
								<MessagePanel currentChannel={currentChannel} />
							</Grid>
							<Grid item xs={3} md={3} lg={3}>
								{channels.length > 0 && <ChannelNotBelong />}
							</Grid>
						</Grid>
					) : (
						<Grid container>
							<Grid item xs={3} md={3} lg={3}></Grid>
							<Grid item xs={6} md={6} lg={6}>
								<ChannelList />
							</Grid>
							<Grid item xs={3} md={3} lg={3}></Grid>
						</Grid>
					)}
				</main>
			</Box>
		</WebSocketProvider>
	);
}
