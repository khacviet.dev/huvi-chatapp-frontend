import {
	Box,
	Button,
	Divider,
	Grid,
	IconButton,
	Modal,
	Paper,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import channelApi from 'api/channelApi';
import InputField from 'custom-fields/InputField';
import { handleChannelsByUserChange } from 'features/Channels/channelSlice';
import { FastField, Form, Formik } from 'formik';
import React, { useContext, useState } from 'react';
import { useDispatch } from 'react-redux';
import { ToastifyContext } from 'utils/ToastifyConfig';
import * as Yup from 'yup';
import './CreateChannelModal.css';
import { useStyles } from './CreateChannelModalStyle';

function CreateChannelModal() {
	const classes = useStyles();
	const notify = useContext(ToastifyContext);
	const dispatch = useDispatch();

	const [open, setOpen] = useState(false);

	const handleOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const initialValues = {
		channelName: '',
	};

	const validationShema = Yup.object().shape({
		channelName: Yup.string().required('Required'),
	});

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={validationShema}
			onSubmit={(values, { resetForm }) => {
				const post = async () => {
					try {
						const response = await channelApi.createChannel(values);
						dispatch(handleChannelsByUserChange());
						resetForm({
							channelName: '',
						});

						handleClose();
					} catch (error) {
						notify('error', error.response.data.message);
						if (error.response.status === 403) {
							history.push('/signin');
						}
					}
				};

				post();
			}}
		>
			{(formikProps) => {
				return (
					<Box style={{ width: '100%' }}>
						<Button
							style={{
								textTransform: 'none',
							}}
							color='primary'
							fullWidth
							variant='contained'
							startIcon={<AddIcon />}
							onClick={handleOpen}
						>
							Create Channel
						</Button>
						<Modal open={open} onClose={handleClose}>
							{
								<Paper className='create-modal'>
									<Form>
										<Grid container alignItems='center' spacing={1}>
											<Grid
												item
												xs={10}
												md={10}
												lg={10}
												container
												justify='center'
											>
												<Box className={classes.createPostTitle}>
													Create Channel
												</Box>
											</Grid>
											<Grid
												item
												xs={2}
												md={2}
												lg={2}
												container
												justify='flex-end'
											>
												<IconButton onClick={handleClose}>
													<CloseIcon />
												</IconButton>
											</Grid>
											<Grid item xs={12} md={12} lg={12}>
												<Divider />
											</Grid>
											<Grid item md={12}>
												<FastField
													name='channelName'
													component={InputField}
													label={'Channel name'}
													fullWidth={true}
													required={true}
												/>
											</Grid>

											<Grid
												item
												xs={12}
												md={12}
												lg={12}
												className={classes.postPanel}
											>
												<Box>
													<Button
														type='submit'
														variant='contained'
														color='primary'
														fullWidth
													>
														Create
													</Button>
												</Box>
											</Grid>
										</Grid>
									</Form>
								</Paper>
							}
						</Modal>
					</Box>
				);
			}}
		</Formik>
	);
}

export default CreateChannelModal;
