import {
	Avatar,
	Button,
	Divider,
	Grid,
	Paper,
	Typography,
} from '@material-ui/core';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import InputIcon from '@material-ui/icons/Input';
import channelApi from 'api/channelApi';
import React, { useContext, useEffect, useState } from 'react';
import { WebSocketContext } from 'utils/socketConfig';
import { ToastifyContext } from 'utils/ToastifyConfig';

function ChannelNotBelong() {
	const socket = useContext(WebSocketContext);
	const notify = useContext(ToastifyContext);

	const [channelsNotBelong, setChannelNotBelong] = useState([]);
	const [requests, setRequests] = useState([]);
	const [isRequestChange, setIsRequestChange] = useState(false);
	const [isChange, setIsChange] = useState(false);

	const handleApproveRequest = (userRequest, channelId) => {
		const post = async () => {
			try {
				const data = {
					userRequest,
				};

				const response = await channelApi.approveRequestChannel(
					data,
					channelId
				);

				socket.emit('approveRequest', userRequest);

				setIsRequestChange(!isRequestChange);
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/signin');
				}
			}
		};

		post();
	};

	const requestChannel = (channelId) => {
		const post = async () => {
			try {
				const response = await channelApi.requestChannel(channelId);
				setIsChange(!isChange);
				socket.emit('requestChannel', channelId);
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/signin');
				}
			}
		};

		post();
	};

	const unRequestChannel = (channelId) => {
		const post = async () => {
			try {
				const response = await channelApi.unRequestChannel(channelId);
				setIsChange(!isChange);
				socket.emit('unRequestChannel', channelId);
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/signin');
				}
			}
		};

		post();
	};

	useEffect(() => {
		const get = async () => {
			try {
				const response = await channelApi.getChannelsNotByUserId();
				setChannelNotBelong(response);
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/signin');
				}
			}
		};

		get();
	}, [isChange]);

	useEffect(() => {
		const get = async () => {
			try {
				const response = await channelApi.getRequestsChannel();
				setRequests(response);
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/signin');
				}
			}
		};

		get();
	}, [isRequestChange]);

	useEffect(() => {
		socket.on('approveRequestResponse', () => {
			setIsChange(!isChange);
		});
	}, []);

	useEffect(() => {
		socket.on('requestChannelResponse', () => {
			setIsRequestChange(!isRequestChange);
			socket.off('requestChannelResponse');
		});

		socket.on('unRequestChannelResponse', () => {
			setIsRequestChange(!isRequestChange);
			socket.off('unRequestChannelResponse');
		});
	}, [isRequestChange]);

	return (
		<Paper
			style={{
				height: 'calc(100vh - 64px)',
				padding: '1.5rem 1rem 1rem 1rem',
				overflowY: 'scroll',
			}}
		>
			{requests.length > 0 && (
				<Grid container style={{ marginBottom: '2rem' }}>
					<Grid item md={12} style={{ marginBottom: '1.5rem' }}>
						<Typography style={{ fontWeight: 'bold' }}>Request</Typography>
					</Grid>
					{requests.map((request, index) => (
						<Grid key={index} item md={12} style={{ marginBottom: '1rem' }}>
							<Grid container alignItems='center'>
								<Grid item md={2}>
									<Avatar src={request.avt_link} />
								</Grid>
								<Grid item md={7}>
									<Typography variant='body2'>
										<b>{request.display_name}</b> want to join channel{' '}
										<b>{request.channel_name}</b>
									</Typography>
								</Grid>
								<Grid item md={3}>
									<Button
										fullWidth
										onClick={() =>
											handleApproveRequest(
												request.user_request_id,
												request.channel_id
											)
										}
										size='small'
										variant='outlined'
										color='primary'
									>
										Approve
									</Button>
								</Grid>
							</Grid>
						</Grid>
					))}
				</Grid>
			)}
			{requests.length > 0 && channelsNotBelong.length > 0 && (
				<Grid container>
					<Divider style={{ width: '100%' }} />
				</Grid>
			)}
			<Grid container style={{ marginTop: '1.5rem' }}>
				{channelsNotBelong.length > 0 && (
					<Grid item md={12} style={{ marginBottom: '1.5rem' }}>
						<Typography style={{ fontWeight: 'bold' }}>
							Other channels
						</Typography>
					</Grid>
				)}
				{channelsNotBelong.length > 0 &&
					channelsNotBelong.map((channel, index) => (
						<Grid
							key={index}
							container
							style={{ marginBottom: '1rem' }}
							alignItems='center'
						>
							<Grid item md={2}>
								<Avatar src={channel.channel_avt} />
							</Grid>
							<Grid item md={7}>
								{channel.channel_name}
							</Grid>
							<Grid item md={3}>
								{channel.is_request ? (
									<Button
										fullWidth
										style={{ textTransform: 'none' }}
										startIcon={<InputIcon />}
										variant='contained'
										color='default'
										size='small'
										onClick={() => unRequestChannel(channel.channel_id)}
									>
										Requested
									</Button>
								) : (
									<Button
										fullWidth
										style={{ textTransform: 'none' }}
										startIcon={<GroupAddIcon />}
										variant='contained'
										size='small'
										color='secondary'
										onClick={() => requestChannel(channel.channel_id)}
									>
										Request
									</Button>
								)}
							</Grid>
						</Grid>
					))}
			</Grid>
		</Paper>
	);
}

export default ChannelNotBelong;
