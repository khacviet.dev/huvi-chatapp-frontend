const { makeStyles } = require('@material-ui/core');

export const useStyles = makeStyles(() => ({
	toolbar: {
		paddingRight: 24,
	},
	title: {
		flexGrow: 1,
		fontWeight: 'bold',
	},
	avatar: {
		cursor: 'pointer',
	},
}));
