import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import userApi from 'api/userApi';

export const getUsersByChannel = createAsyncThunk(
	'/user/getUsersByChannel',
	async (channelId) => {
		const response = await userApi.getUsersByChannel(channelId);
		return response;
	}
);

export const getMe = createAsyncThunk('/user/getMe', async () => {
	const response = await userApi.getMe();
	return response;
});

const userSlice = createSlice({
	name: 'users',
	initialState: {
		channelUsers: [],
		me: {},
		loading: false,
		error: '',
	},
	reducers: {},
	extraReducers: {
		[getUsersByChannel.pending]: (state) => {
			state.loading = true;
		},
		[getUsersByChannel.rejected]: (state, action) => {
			state.error = action.error;
		},
		[getUsersByChannel.fulfilled]: (state, action) => {
			state.loading = false;
			state.channelUsers = action.payload;
		},
		[getMe.fulfilled]: (state, action) => {
			state.me = action.payload;
		},
	},
});

const { reducer: userReducer } = userSlice;
export default userReducer;
