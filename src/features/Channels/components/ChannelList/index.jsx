import {
	Avatar,
	Button,
	Grid,
	InputAdornment,
	Paper,
	TextField,
	Typography,
} from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import InputIcon from '@material-ui/icons/Input';
import SearchIcon from '@material-ui/icons/Search';
import channelApi from 'api/channelApi';
import { handleChannelsByUserChange } from 'features/Channels/channelSlice';
import React, { useContext, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { WebSocketContext } from 'utils/socketConfig';
import { ToastifyContext } from 'utils/ToastifyConfig';
import CreateChannelModal from '../CreateChannelModal';

function ChannelList() {
	const notify = useContext(ToastifyContext);
	const socket = useContext(WebSocketContext);
	const dispatch = useDispatch();

	const [isLoading, setIsLoading] = useState(false);
	const [channels, setChannels] = useState([]);
	const [isChange, setIsChange] = useState(false);

	const requestChannel = (channelId) => {
		const post = async () => {
			try {
				const response = await channelApi.requestChannel(channelId);
				setIsChange(!isChange);
				socket.emit('requestChannel', channelId);
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/signin');
				}
			}
		};

		post();
	};

	const unRequestChannel = (channelId) => {
		const post = async () => {
			try {
				const response = await channelApi.unRequestChannel(channelId);
				setIsChange(!isChange);
				socket.emit('unRequestChannel', channelId);
			} catch (error) {
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/signin');
				}
			}
		};

		post();
	};

	useEffect(() => {
		const get = async () => {
			try {
				setIsLoading(true);
				const response = await channelApi.getChannels();
				setIsLoading(false);
				setChannels(response);
			} catch (error) {
				setIsLoading(false);
				notify('error', error.response.data.message);
				if (error.response.status === 403) {
					history.push('/signin');
				}
			}
		};

		get();
	}, [isChange]);

	useEffect(() => {
		socket.on('approveRequestResponse', () => {
			dispatch(handleChannelsByUserChange());
		});
	}, []);

	return (
		<Paper
			style={{
				height: '700px',
				marginTop: '5rem',
				padding: '2rem 1rem 1rem 1rem',
				overflowY: 'scroll',
			}}
		>
			{isLoading && (
				<div
					style={{
						position: 'fixed',
						top: '50%',
						left: '50%',
						transform: 'translate(-50%, -50%)',
						zIndex: '1000000000',
					}}
					className='cp-spinner cp-bubble'
				></div>
			)}
			<Grid container spacing={2}>
				<Grid item md={9}>
					<TextField
						InputProps={{
							startAdornment: (
								<InputAdornment position='start'>
									<SearchIcon style={{ color: '#aaa' }} />
								</InputAdornment>
							),
						}}
						variant='outlined'
						size='small'
						label='Undeveloped'
						fullWidth
					/>
				</Grid>
				<Grid item md={3}>
					<Grid container justifyContent='flex-end'>
						<CreateChannelModal handleCreateChannel={() => {}} />
					</Grid>
				</Grid>
			</Grid>
			<Grid container style={{ marginTop: '3rem' }}>
				{channels.length > 0 ? (
					<TableContainer component={Paper}>
						<Table stickyHeader>
							<TableHead>
								<TableRow>
									<TableCell align='center'>
										<Typography style={{ fontWeight: 'bold' }}>
											Channel Image
										</Typography>
									</TableCell>
									<TableCell align='center'>
										<Typography style={{ fontWeight: 'bold' }}>
											Channel Name
										</Typography>
									</TableCell>
									<TableCell align='center'>
										<Typography style={{ fontWeight: 'bold' }}>
											Total Members
										</Typography>
									</TableCell>
									<TableCell align='center'></TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{channels.map((channel, index) => (
									<TableRow hover key={index}>
										<TableCell component='th' scope='row'>
											<Grid container justifyContent='center'>
												<Avatar
													style={{ width: '5rem', height: '5rem' }}
													src={channel.channel_avt}
												/>
											</Grid>
										</TableCell>
										<TableCell align='center'>{channel.channel_name}</TableCell>
										<TableCell align='center'>{channel.count}</TableCell>
										<TableCell align='center'>
											{channel.is_request ? (
												<Button
													style={{ textTransform: 'none' }}
													startIcon={<InputIcon />}
													variant='contained'
													color='default'
													onClick={() => unRequestChannel(channel.channel_id)}
												>
													Requested
												</Button>
											) : (
												<Button
													style={{ textTransform: 'none' }}
													startIcon={<GroupAddIcon />}
													variant='contained'
													color='secondary'
													onClick={() => requestChannel(channel.channel_id)}
												>
													Request
												</Button>
											)}
										</TableCell>
									</TableRow>
								))}
							</TableBody>
						</Table>
					</TableContainer>
				) : (
					<Grid container justifyContent='center'>
						<Typography variant='h6'>Not channel found</Typography>
					</Grid>
				)}
			</Grid>
		</Paper>
	);
}

export default ChannelList;
