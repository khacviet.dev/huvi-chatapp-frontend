import {
	AppBar,
	Avatar,
	Box,
	Grid,
	ListItemIcon,
	ListItemText,
	Menu,
	MenuItem,
	Toolbar,
	Typography,
} from '@material-ui/core';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import authApi from 'api/authApi';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { isEmpty } from 'utils/function';
import { useStyles } from './MyAppBarStyle';

MyAppBar.propTypes = {
	me: PropTypes.object.isRequired,
};

export default function MyAppBar(props) {
	const { me } = props;
	const classes = useStyles();
	let history = useHistory();

	const [anchorEl, setAnchorEl] = useState(null);

	const changeLocation = (path) => {
		history.push(`/${path}`);
	};

	const logout = () => {
		const handleLogout = async () => {
			await authApi.signout();
			history.push('/signin');
		};
		handleLogout();
	};

	const handleClick = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handleCloseMenu = () => {
		setAnchorEl(null);
	};

	return (
		<AppBar position='fixed'>
			<Toolbar className={classes.toolbar}>
				<Grid container>
					<Grid item xs={6} md={6} lg={6}>
						<Typography
							onClick={() => changeLocation('')}
							variant='h6'
							noWrap
							className={classes.title}
						>
							Huvi
						</Typography>
					</Grid>
				</Grid>
				<Grid item xs={6} md={6} lg={6}>
					<Grid container>
						<Grid
							item
							xs={12}
							md={12}
							lg={12}
							container
							justify='flex-end'
							alignItems='center'
						>
							{!isEmpty(me) && (
								<Avatar
									src={me.avt_link}
									className={classes.avatar}
									onClick={handleClick}
								/>
							)}
							<Menu
								anchorEl={anchorEl}
								keepMounted
								open={Boolean(anchorEl)}
								onClose={handleCloseMenu}
							>
								<Box onClick={logout}>
									<MenuItem onClick={handleCloseMenu}>
										<ListItemIcon>
											<ExitToAppIcon fontSize='small' />
										</ListItemIcon>
										<ListItemText primary='Logout' />
									</MenuItem>
								</Box>
							</Menu>
						</Grid>
					</Grid>
				</Grid>
			</Toolbar>
		</AppBar>
	);
}
