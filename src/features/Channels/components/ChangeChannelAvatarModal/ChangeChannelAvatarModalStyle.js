const { makeStyles } = require('@material-ui/core');

export const useStyles = makeStyles((theme) => ({
	modal: {
		position: 'absolute',
		top: '50%',
		left: '50%',
		transform: 'translate(-50%,-50%)',
		padding: theme.spacing(1.5),
		width: 500,
		outline: 0,
	},
	createPostTitle: {
		marginLeft: '57px',
		fontSize: '1.25rem',
		fontWeight: 'bold',
	},
	browserImageButton: {
		cursor: 'pointer',
	},
	image: {
		width: '100%',
		height: 'auto',
	},
	closeImageButton: {
		position: 'absolute',
		top: '10px',
		right: '10px',
		backgroundColor: '#fff',
	},
}));
